<?php

/**
 * Implementation of hook_form_alter().
 *
 * Add our own submit handler to the gateway settings form so that we can
 * encrypt them before saving them.
 *
 * @todo: Is it really necessary to encrypt this stuff? Ah well.
 * @see: uc_gmo_pg_payment_gateway_settings_submit()
 */
function uc_gmo_pg_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'uc_payment_gateways_form') {
    $form['#submit'][] = 'uc_gmo_pg_payment_gateway_settings_submit';
  }
}

/**
 * Submit handler to encrypt merchant log in information.
 */
function uc_gmo_pg_payment_gateway_settings_submit($form, &$form_state) {
  // If CC encryption has been configured properly.
  if ($key = uc_credit_encryption_key()) {
    // Setup our encryption object.
    $crypt = new uc_encryption_class;

    // Encrypt the Merchant information.
    if (!empty($form_state['values']['uc_gmo_pg_access_id'])) {
      variable_set('uc_gmo_pg_access_id', $crypt->encrypt($key, $form_state['values']['uc_gmo_pg_access_id']));
    }

    if (!empty($form_state['values']['uc_gmo_pg_access_pass'])) {
      variable_set('uc_gmo_pg_access_pass', $crypt->encrypt($key, $form_state['values']['uc_gmo_pg_access_pass']));
    }

    // Store any errors.
    uc_store_encryption_errors($crypt, 'uc_gmo_pg');
  }
}

/**
 * Implementation of hook_payment_gateway(), an Ubercart hook.
 */
function uc_gmo_pg_payment_gateway() {
  $gateways[] = array(
    'id' => 'gmo_pg',
    'title' => t('GMO-PG'),
    'description' => t('Process credit card payments using the GMO-PG gateway.'),
    'settings' => 'uc_gmo_pg_settings_form',
    'credit' => 'uc_gmo_pg_charge',
    'credit_txn_types' => array(UC_CREDIT_PRIOR_AUTH_CAPTURE, UC_CREDIT_AUTH_CAPTURE, UC_CREDIT_REFERENCE_TXN),
  );

  return $gateways;
}

/**
 * Implementation of hook_settings_form(), an Ubercart hook.
 *
 * Adds the GMO-PG fields to the payment gateway settings form.
 */
function uc_gmo_pg_settings_form() {

  $login = _uc_gmo_pg_login_data();
  $form = array('gmo_pg' => array());

  $form['gmo_pg']['uc_gmo_pg_access_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Shop ID'),
    '#default_value' => $login['access_id'],
    '#weight' => 100,
  );
  $form['gmo_pg']['uc_gmo_pg_access_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Shop password'),
    '#default_value' => $login['access_pass'],
    '#weight' => 110,
  );
  $form['gmo_pg']['uc_gmo_pg_access_server'] = array(
    '#type' => 'radios',
    '#title' => t('API server'),
    '#options' => array(
      'live' => t('Live'),
      'sandbox' => t('Sandbox'),
    ),
    '#default_value' => variable_get('uc_gmo_pg_access_server', 'sandbox'),
    '#weight' => 120,
  );
  return $form;
}

/**
 * Implementaiton of hook_charge(), an Ubercart hook.
 *
 * Send data to the processor to charge the card.
 */
function uc_gmo_pg_charge($order_id, $amount, $data) {
  global $user;
  $creds = _uc_gmo_pg_login_data();
  $use_sandbox = variable_get('uc_gmo_pg_access_server', 'sandbox') === 'live' ? FALSE : TRUE;
  $order = uc_order_load($order_id);
  $bget_path = libraries_get_path('bget');
  require $bget_path . '/includes/Bget.class.inc';
  require $bget_path . '/includes/BgetHttp.class.inc';
  module_load_include('inc', 'uc_gmo_pg', 'BgetGmo.class');
  $bget = new BgetGmo('EntryTran');
  $bget->setSandboxUsage($use_sandbox)->setPostFields(array(
    'ShopID' => $creds['access_id'],
    'ShopPass' => $creds['access_pass'],
    'OrderID' => $order_id,
    'JobCd' => 'AUTH',
    'ItemCode' => '0000000',
    'Amount' => round($amount),
    'Tax' => 0,
    'TdFlag' => /* $user->uid */ 0,
    'TdTenantName' => base64_encode($user->name),
    'User' => 'MODP-3.107.105',
    'Version' => 105,
  ));
  $return = $bget->execute()->getGmoResponseData();
  if (isset($return['ErrCode'])) {
    if ($return['ErrInfo'] === 'E01040010') {
      // This is the "order number already exists" error. We'll query the server
      // for the access ID and pass.
      $bget = new BgetGmo('SearchTrade');
      $bget->setSandboxUsage($use_sandbox)->setPostFields(array(
        'ShopID' => $creds['access_id'],
        'ShopPass' => $creds['access_pass'],
        'OrderID' => $order_id,
        'User' => 'MODP-3.107.105',
        'Version' => 105,
      ));
      $return = $bget->execute()->getGmoResponseData();
    }
    else {
      watchdog('uc_gmo_pg', 'GMO server returned an error. Error code: !code; Error info code: !info. Check the &ldquo;エラーコード表&rdquo; documentation file for an explanation.', array('!code' => $return['ErrCode'], '!info' => $return['ErrInfo']));
      return array(
        'success' => FALSE,
        'comment' => t('UC GMO PG'),
        'message' => t('The GMO server returned an error.'),
        'uid' => $user->uid,
      );
    }
  }
  $bget = new BgetGmo('ExecTran');
  $bget->setSandboxUsage($use_sandbox)->setPostFields(array(
    'AccessID' => $return['AccessID'],
    'AccessPass' => $return['AccessPass'],
    'OrderID' => $order_id,
    'Method' => 1,
    'PayTimes' => '',
    'CardNo' => $order->payment_details['cc_number'],
    // Format for expiry date is MMYY
    'Expire' => str_pad($order->payment_details['cc_exp_month'], 2, '0', STR_PAD_LEFT) . substr($order->payment_details['cc_exp_year'], 2),
    'SecurityCode' => $order->payment_details['cc_cvv'],
    // Pass through browser request info. Why? I donno, but it's what the
    // official library does.
    'HttpAccept' => $_SERVER['HTTP_ACCEPT'],
    'HttpUserAgent' => $_SERVER['HTTP_USER_AGENT'],
    'DeviceCategory' => 0,
    'User' => 'MODP-3.107.105',
    'Version' => 105,
  ));
  $return = $bget->execute()->getGmoResponseData();
  if (isset($return['ErrCode'])) {
    watchdog('uc_gmo_pg', 'GMO server returned an error. Error code: !code; Error info code: !info. Check the &ldquo;エラーコード表&rdquo; documentation file for an explanation.', array('!code' => $return['ErrCode'], '!info' => $return['ErrInfo']));
    return array(
      'success' => FALSE,
      'comment' => t('UC GMO PG'),
      'message' => t('The GMO server returned an error.'),
      'uid' => $user->uid,
    );
  }
  else {
    return array(
      'success' => TRUE,
      'comment' => t('UC GMO PG'),
      'message' => t('The GMO payment process completed successfully.'),
      'uid' => $user->uid,
    );
  }
}

/**
 * Fetch unencrypted login credentials.
 *
 * @return array
 *   An array, keyed by 'access_id' and 'access_pass', with respective values.
 */
function _uc_gmo_pg_login_data() {
  static $data;

  if (!empty($data)) {
    return $data;
  }

  $access_id = variable_get('uc_gmo_pg_access_id', '');
  $access_pass = variable_get('uc_gmo_pg_access_pass', '');

  // If CC encryption has been configured properly.
  if ($key = uc_credit_encryption_key()) {
    // Setup our encryption object.
    $crypt = new uc_encryption_class;

    // Decrypt the Merchant ID and Transaction key.
    if (!empty($access_id)) {
      $access_id = $crypt->decrypt($key, $access_id);
    }

    if (!empty($access_pass)) {
      $access_pass = $crypt->decrypt($key, $access_pass);
    }

    // Store any errors.
    uc_store_encryption_errors($crypt, 'uc_gmo_pg');
  }

  $data = array(
    'access_id' => $access_id,
  	'access_pass' => $access_pass,
  );
  return $data;
}
