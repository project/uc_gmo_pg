Ubercart GMO Payment Gateway
By Garrett Albright and Alethia
http://www.alethia-inc.com/

This is a module allowing one to accept credit card payments on an Ubercart 2
web store (using Drupal 6) with the Japan-based GMO Payment Gateway.

Note that while GMO supports many forms of payment including bank transfer,
bill payment at a convenience store, etc, only credit card payments are
supported by this module currently.

== Installation ==

This module depends on the Better Getter PHP library for communicating with
GMO's servers. Download the library at https://github.com/GoingOn/bget (if you
are not familiar with Git, look for the "Downloads" button near the upper-left
corner of the page to download a simple archive) and place the files in a
directory at sites/all/libraries/bget (note that you may have to create the
parent sites/all/libraries directory).

This module also depends on the Libraries module, which you can download at
http://drupal.org/project/libraries

Once those requirements are fulfilled, install and enable this module like you
would any other.

Find the Shop ID and Shop Password for your store. To do this, log in to the GMO
back-end for your account, click on the ショップの管理 tab, then click on ショップ情報 in the
submenu. Look for the ショップID and ショップパスワード.

Now, in your Drupal site, head to Administration > Store administration >
Configuration > Payment settings. Click on the "Edit" tab, then the "Payment
methods" subtab. Check the box to enable the "Credit card" payment method (if
not enabled already) and set "GMO-PG" as the default gateway. Save the settings.

Back on that same page, open up the "Credit card settings" fieldset and
configure as necessary.

Finally, click on the "Payment gateways" subtab and open the "GMO-PG settings"
fieldset. Enter the respective values into the  "Shop ID" and "Shop password"
fields. From there, you should be good to go.
