<?php

/**
 * Extension of Better Getter HTTP class specially for the GMO payment
 * proessor.
 */
class BgetGmo extends BgetHttp {

  /**
   * Parsed response data from the GMO server.
   */
  protected $gmoResponseData;

  /**
   * Are we using the test server URI?
   */
  protected $useSandboxServer = FALSE;

  /**
   * Get the sandbox server usage value.
   *
   * @return
   *   TRUE if the test server URI will be used for queries; FALSE otherwise.
   */
  function getSandboxUsage() {
    return (bool)$this->useSandboxServer;
  }

  /**
   * Set the sandbox server usage value.
   *
   * If set to TRUE, BgetGmo::getUri() will return a URL for the query test
   * server IF AND ONLY IF one of the shortcut paths ('EntryTran', 'ExecTran',
   * 'SearchTrade') are used with setUri().
   *
   * @see BgetGmo::getUri()
   *
   * @param $setting
   *   A boolean value representing whether the test server will be for queries;
   *   TRUE if so, FALSE otherwise.
   * @return
   *   $this, for chaining.
   */
  function setSandboxUsage($setting) {
    $this->useSandboxServer = $setting;
    return $this;
  }

  /**
   * Return the parsed response data from the GMO server.
   *
   * If a query has executed but the response has not yet been parsed, this
   * will parse the response.
   *
   * @return
   *   An array of parsed response data, or NULL if there's no response to
   *   parse yet.
  */
  function getGmoResponseData() {
    if (empty($this->gmoResponseData)) {
      $response = $this->getResponseBody();
      if ($response !== NULL) {
        // parse_str() is an odd best; a product of the nightmarish days of old
        // (at least as far as PHP is concerned). As evidence, it needs a
        // variable passed to it which it will populate with values, sort of
        // like preg_match(). Why doesn't it just return such an array as its
        // return value? Good question, because if you use parse_str() without
        // that second parameter, it creates variables in the global namespace!
        // I'm sure that was a great idea back in the day when everybody on the
        // internet was completely, 100% trustworthy (so never).
        parse_str($response, $this->gmoResponseData);
      }
    }
    return $this->gmoResponseData;
  }

  /**
   * Override the prepareRequest() method to deal with the way that GMO wants
   * the POST data.
   *
   * We want to set the POST data as our own built string because if we do it
   * the normal way, cURL wants to send the data using the multipart/form-data
   * enctype, which the server does *not* like.
   */
  function prepareRequest() {
    // Only go ahead with this if raw post data isn't already being used.
    if (!$this->getRawPostData()) {
      $this->setRawPostData(http_build_query($this->getPostFields(), '', '&'));
    }
    return parent::prepareRequest();
  }

  /**
   * Override getUri() so that we can provide some handy shortcuts for setting
   * the URI and change the "real" URI depending on whether we want to return
   * a URI to the test server or not.
   */
  function getUri() {
    $uri = parent::getUri();
    if (in_array($uri, array('EntryTran', 'ExecTran', 'SearchTrade'))) {
      // Note the difference between sandbox and live URLs; sandbox ones start
      // with pt01, whereas live ones start with p01.
      if ($this->getSandboxUsage()) {
        return "http://pt01.mul-pay.jp/payment/{$uri}.idPass";
      }
      else {
        return "http://p01.mul-pay.jp/payment/{$uri}.idPass";
      }
    }
    else {
      return $uri;
    }
  }
}
